INTRODUCTION
------------

Smart Content Demandbase is a connector module to allow fields available through Demandbase’s API to be used as
conditions in Smart Content.

Note: Use of this module requires an active subscription to Demandbase’s ABM Platform and Engagement Solution.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/smart_content_demandbase

 * To submit bug reports, feature suggestions or to track changes:
   https://www.drupal.org/project/issues/smart_content_demandbase


REQUIREMENTS
------------

This module requires the following modules:

 * Smart Content (https://www.drupal.org/project/smart_content)


INSTALLATION
------------

Install using composer: composer require drupal/smart_content_demandbase


CONFIGURATION
-------------

This module does not require any configuration. However, please note that the Demandbase tag must be added to your site
in some manner. You could either:
 * Use the contributed Demandbase module (https://www.drupal.org/project/demandbase); or
 * Add the tag via Google Tag Manager
   (https://support.demandbase.com/hc/en-us/articles/214477283-Deploy-via-Google-Tag-Manager-GTM-)


MAINTAINERS
-----------

Current maintainers:
 * Michael Lander (michaellander) - https://www.drupal.org/u/michaellander
 * Gurwinder Antal (gantal) - https://www.drupal.org/u/michaellander

This project has been sponsored by:

  * Elevated Third
    Empowering B2B marketing ecosystems with strategic thinking, top-notch user experience design and world-class Drupal
    development.
