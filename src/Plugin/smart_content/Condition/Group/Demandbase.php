<?php


namespace Drupal\smart_content_browser\Plugin\smart_content\Condition\Group;

use Drupal\smart_content\Condition\Group\ConditionGroupBase;

/**
 * Provides a condition group for Demandbase conditions.
 *
 * @SmartConditionGroup(
 *   id = "demandbase",
 *   label = @Translation("Demandbase")
 * )
 */
class Demandbase extends ConditionGroupBase {

}
