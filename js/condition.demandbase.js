(function (Drupal) {
  Drupal.smartContent = Drupal.smartContent || {};
  Drupal.smartContent.plugin = Drupal.smartContent.plugin || {};
  Drupal.smartContent.plugin.Field = Drupal.smartContent.plugin.Field || {};

  Drupal.smartContent.plugin.Field['demandbase'] = function (condition) {
    let key = condition.field.pluginId.split(':')[1];
    if(!Drupal.smartContent.hasOwnProperty('demandbase')) {
      // todo: decide if this should update behind the scenes.
      if(!Drupal.smartContent.storage.isExpired('demandbase')) {
        let values = Drupal.smartContent.storage.getValue('demandbase');
        Drupal.smartContent.demandbase  = values;
      }
      else {
        Drupal.smartContent.demandbase = new Promise((resolve, reject) => {
          let attempts = 0;
          // todo: Give the interval more time to complete, but still resolve early
          //   if delayed too long.
          const interval =  setInterval(() => {
            if (attempts < 200) {
              if (window.Demandbase && window.Demandbase.IP && window.Demandbase.IP.CompanyProfile && window.Demandbase.IP.CompanyProfile.ip) {
                clearInterval(interval);
                Drupal.smartContent.storage.setValue('demandbase', window.Demandbase.IP.CompanyProfile);
                resolve(window.Demandbase.IP.CompanyProfile);
              }
            }
            else {
              clearInterval(interval);
              resolve({});
            }
            attempts++;
          }, 20);
        });
      }

    }
    return Promise.resolve(Drupal.smartContent.demandbase).then( (value) => {
      if(value.hasOwnProperty(key)) {
        return value[key];
      }
      else {
        return null;
      }
    });
  }

}(Drupal));
